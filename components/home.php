 <h1 style="font-size:50px; font-family: verdana; text-align:center;">Moja prva stranica</h1>
    <!--Naslov, sto je veci broj "h" to je text , a sa style="font-size:brojpx"; povecavam velicinu fonta.-->

    <p><a href="https://www.w3schools.com" target="_blank">Link</a> stranice s koje učim.</p>
    <!--u "<a href="link stranice">text koji ce biti link-->
    
    <div class="inline-text">
        <h2 style="font-family:verdana; background-color: green">Sve što sam prošao do sada o HTML-u</h2>
    </div>
    
    <ul>
        <!--Pomeri text od ruba (kao kad stisneš tab u wordu itd.) i stavi • iza texta.-->
    
        <li>Home</li>
        <li>Introduction</li>
        <li>Editors</li>
        <li>Basic</li>
    
    </ul>
    
    <hr>
    <!--Odvaja content vodoravnom crtom-->
    
    <div class="inline-text">
        <h2 style="font-family:courier; background-color: red">Sve što mi je ostalo o HTML-u</h2>
    </div>
    
    <ol>
        <!--Pomeri text od ruba i stavi redne brojeve iza texta-->
    
        <li>Elements</li>
        <li>Atributes</li>
        <li>Headings</li>
    
    </ol>

    <hr>

    <div class="inline-text">
        <p style="color:red; border:3px solid grey; padding: 10px;" title="Ja sam title atribut"><b>Ovaj paragraf je u boji i stavi mis preko njega
                da ti prikaze naslov paragrafa.
            </b></p>
    </div>
    <!--<b>Podeblja text</b>-->
    
    <p>U ovom paragrafu koristimo <b>break</b><br>da bi text presao u drugi red.</p>
    
    <pre>   <!--Odvoji od ruba i kad koristis enter u njemu tako bude i na stranici-->
    
        Ja sam ja.
    
        Ti si ti.
    
        Mi smo mi.
    
        </pre>
    <p><i>Ovaj text je ukošen.</i></p>
    <p>Ovaj text ima<sub> subscript</sub> i <sup>supersript</sup>.</p>
    <p><strong>Ovo je važno.</strong></p>
    <p><em>Ovo je em.</em></p>
    <p>Ovo je <mark>označen</mark> text.</p>
    <p>Moja omiljena boje je <del>crvena</del> plava.</p>
    <p>Moja omiljena <ins>boja</ins> je plava.</p>

    <h1>Fortnite stats</h2>
    <?php require "./table.php" ?>