<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Register</title> <!--Naziv kartice na npr. Google Chrome-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>



 <div class="wrapper">
<div class="container">

<?php require "./components/nav.php" ?>



<?php require "./components/register.php" ?>


</div>
 </div>
</body>
</html>