<?php 

$routeUrl = $_SERVER['REQUEST_URI'];

$parts = explode("/", $routeUrl);

$currentRoute = end($parts);

?>

<header>
    <div class="container">
        <div id="branding">
        <h1><span class="highlight">Acme</span> Web Design</h1>
        </div>
        <div>
        <nav>
            <ul>
                <li class="<?php if($currentRoute === 'index.php'){echo 'active';} ?>"><a href="index.php">Home</a></li>
                <li class="<?php if($currentRoute === 'about.php'){echo 'active';} ?>"><a href="about.php">About</a></li>
                <li class="<?php if($currentRoute === 'services.php'){echo 'active';} ?>"><a href="services.php">Services</a></li>
            </ul>
        </nav>
    </div>
</header>