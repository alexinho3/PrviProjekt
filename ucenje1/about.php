<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Affordable and professional web design">
    <meta name="keywords" content="web design, affordable web design, professional web design">
    <meta name="author" content="Alex Dragun">
    <title>Acme Web Design | About</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>

    <?php require "nav.php" ?>

    <?php require "newsletterEmail.php" ?>

    <section id="main">
        <div class="container">
           <article id="main-col">
                <h1 class="page-title">About Us</h1>
                <p>Fusce pellentesque elit nec finibus sollicitudin. Phasellus malesuada porttitor sapien, nec dapibus ex pharetra a. Aliquam
                fermentum nec ipsum nec hendrerit. Duis vulputate eget orci vitae laoreet. Donec vel lacus enim. Nunc eu condimentum lectus.
                </p>
                <p>
                Sed at viverra lacus. Donec ac aliquet sem. Morbi porta, erat sed volutpat tincidunt, leo urna rhoncus augue, nec varius
                ex odio eget lectus. Sed non massa non justo molestie posuere. Suspendisse vel commodo metus. Morbi a leo at erat suscipit
                congue non quis urna. Donec diam risus, maximus ac porttitor nec, facilisis eget ante.
                </p>
           </article> 

           <aside id="sidebar">
               <div class="dark">
               <h3>What We Do</h3>
                <p>Sed at viverra lacus. Donec ac aliquet sem. Morbi porta, erat sed volutpat tincidunt, leo urna rhoncus augue, nec varius
                ex odio eget lectus.</p>
                </div>
           </aside>
        </div>
    </section>

    <?php require "footer.php" ?>
</body>

</html>